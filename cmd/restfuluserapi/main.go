package main

import (
	"restful-user-api/internal"
)

func main() {
	app := internal.NewApp()
	app.InitializeDb()
	app.InitializeRoutes()
	app.Run(":9000")
}