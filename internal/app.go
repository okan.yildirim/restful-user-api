package internal

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"log"
	"net/http"
	"restful-user-api/pkg/controller"
	"restful-user-api/pkg/repository"
	"restful-user-api/pkg/service"
)

type App struct {
	Router *mux.Router
	DB     *gorm.DB
}

func NewApp() *App {
	return &App{
		Router: mux.NewRouter(),
	}
}

func (app *App) InitializeDb() {
	var err error
	app.DB, err = gorm.Open("postgres", pgConnStr)

	if err != nil {
		log.Fatalln(err)
	}
}

func (app *App) InitializeRoutes() {
	userController := app.initController()
	app.Router.HandleFunc("/_monitoring/live", app.MonitoringInfo).Methods("GET")
	app.Router.HandleFunc("/users", userController.CreateUsers).Methods("POST")
	app.Router.HandleFunc("/users", userController.GetUsers).Methods("GET")
	app.Router.HandleFunc("/users/{id}", userController.GetUser).Methods("GET")
	app.Router.HandleFunc("/users/{id}", userController.UpdateUser).Methods("PUT")
	app.Router.HandleFunc("/users/{id}", userController.DeleteUser).Methods("DELETE")
}

func (app *App) initController() controller.UserController {
	userRepository := repository.NewUserRepository(app.DB)
	userService := service.NewUserService(userRepository)
	userController := controller.NewUserController(userService)
	userController.MigrateUsers()
	return userController
}

func (app *App) MonitoringInfo(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	err := json.NewEncoder(w).Encode("OK")
	if err != nil {
		log.Printf("Cannot format json , err=%v\n", err)
	}
}

func (app *App) Run(addr string) {
	fmt.Printf("Server started at %s\n", addr)
	log.Fatal(http.ListenAndServe(addr, app.Router))
}
