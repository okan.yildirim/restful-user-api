package internal

import "fmt"

var (
	dbUsername = "postgres"
	dbPassword = "123qwe"
	dbHost     = "localhost"
	dbTable    = "users"
	dbPort     = "5432"
	pgConnStr  = fmt.Sprintf("host=%s port=%s user=%s password=%s sslmode=disable", dbHost, dbPort, dbUsername, dbPassword)
)
