package controller

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"restful-user-api/pkg/model"
	"restful-user-api/pkg/service"
	"strconv"
)

type UserController struct {
	userService service.UserService
}

func NewUserController(userService service.UserService) UserController {
	return UserController{userService: userService}
}

func (c *UserController) CreateUsers(w http.ResponseWriter, r *http.Request) {
	var userRequest model.UserRequest
	err := json.NewDecoder(r.Body).Decode(&userRequest)
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	createdUser, err := c.userService.CreateUser(&userRequest)
	if err != nil {
		RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	RespondWithJSON(w, http.StatusCreated, createdUser)
}

func (c *UserController) GetUsers(w http.ResponseWriter, r *http.Request) {
	userResponses, err := c.userService.GetUsers()
	if err != nil {
		RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	RespondWithJSON(w, http.StatusOK, userResponses)
}

func (c *UserController) GetUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	userResponse, err := c.userService.GetUser(uint(id))
	if err != nil {
		RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	RespondWithJSON(w, http.StatusOK, userResponse)
}

func (c *UserController) DeleteUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	err = c.userService.DeleteUser(uint(id))
	if err != nil {
		RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	RespondWithJSON(w, http.StatusNoContent, nil)
}

func (c *UserController) UpdateUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	var userRequest model.UserRequest

	err = json.NewDecoder(r.Body).Decode(&userRequest)
	if err != nil {
		RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	userResponse, err := c.userService.UpdateUser(uint(id), &userRequest)
	if err != nil {
		RespondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	RespondWithJSON(w, http.StatusOK, userResponse)
}

func (c *UserController) MigrateUsers() {
	err := c.userService.MigrateUser()
	if err != nil {
		log.Fatalln(err.Error())
	}
}
