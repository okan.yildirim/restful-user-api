package controller

import (
	"encoding/json"
	"net/http"
)

type ErrorResponse struct {
	Code    int      `json:"code"`
	Status  string   `json:"status"`
	Message string   `json:"message"`
	Errors  []string `json:"errors"`
}

func RespondWithError(w http.ResponseWriter, code int, message string) {
	errorResponse := ErrorResponse{
		Code:    code,
		Status:  "Error",
		Message: message,
	}
	RespondWithJSON(w, code, errorResponse)
}

func RespondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
