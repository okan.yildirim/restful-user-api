package service

import (
	"github.com/stretchr/testify/assert"
	"restful-user-api/pkg/model"
	"restful-user-api/pkg/repository"
	"testing"
)

func TestUserService_CreateUser(t *testing.T) {
	userService := NewUserService(repository.NewMockRepository())

	request := model.UserRequest{
		Name:     "okan",
		LastName: "yildirim",
		Age:      27,
	}

	user, err := userService.CreateUser(&request)
	assert.Nil(t, err)
	assert.NotNil(t, user)
	assert.Equal(t, request.Name, user.Name)
	assert.Equal(t, request.LastName, user.LastName)
	assert.Equal(t, request.Age, user.Age)
}

func TestUserService_CreateUser_WhenErrorOccurred(t *testing.T) {
	userService := NewUserService(repository.NewMockRepository())

	request := model.UserRequest{
		Name:     "error",
		LastName: "yildirim",
		Age:      27,
	}

	user, err := userService.CreateUser(&request)
	assert.NotNil(t, err)
	assert.Nil(t, user)
	assert.Equal(t, "error user crud operation", err.Error())
}

func TestUserService_GetUsers(t *testing.T) {
	userService := NewUserService(repository.NewMockRepository())

	users, err := userService.GetUsers()
	assert.Nil(t, err)
	assert.Empty(t, users)

	request := model.UserRequest{
		Name:     "okan",
		LastName: "yildirim",
		Age:      27,
	}

	user, err := userService.CreateUser(&request)
	assert.Nil(t, err)
	assert.NotNil(t, user)

	users2, err := userService.GetUsers()
	assert.Nil(t, err)
	assert.NotEmpty(t, users2)
	assert.Equal(t, 1, len(*users2))

	assert.Equal(t, request.Name, (*users2)[user.Id].Name)
	assert.Equal(t, request.LastName, (*users2)[user.Id].LastName)
	assert.Equal(t, request.Age, (*users2)[user.Id].Age)

}

func TestUserService_GetUser(t *testing.T) {
	userService := NewUserService(repository.NewMockRepository())

	request := model.UserRequest{
		Name:     "okan",
		LastName: "yildirim",
		Age:      27,
	}

	user, err := userService.CreateUser(&request)
	assert.Nil(t, err)
	assert.NotNil(t, user)

	fetchedUser, err := userService.GetUser(user.Id)
	assert.Nil(t, err)
	assert.NotNil(t, fetchedUser)

	assert.Equal(t, request.Name, fetchedUser.Name)
	assert.Equal(t, request.LastName, fetchedUser.LastName)
	assert.Equal(t, request.Age, fetchedUser.Age)
}

func TestUserService_GetUser_WhenErrorOccurred(t *testing.T) {
	userService := NewUserService(repository.NewMockRepository())

	fetchedUser, err := userService.GetUser(5)
	assert.NotNil(t, err)
	assert.Nil(t, fetchedUser)

	assert.Equal(t, "error user crud operation", err.Error())
}

func TestUserService_DeleteUser(t *testing.T) {
	userService := NewUserService(repository.NewMockRepository())

	request := model.UserRequest{
		Name:     "okan",
		LastName: "yildirim",
		Age:      27,
	}

	user, err := userService.CreateUser(&request)
	assert.Nil(t, err)
	assert.NotNil(t, user)

	err = userService.DeleteUser(user.Id)
	assert.Nil(t, err)

	_, err = userService.GetUser(user.Id)
	assert.NotNil(t, err)

}
