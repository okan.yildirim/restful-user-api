package service

import (
	"restful-user-api/pkg/model"
	"restful-user-api/pkg/repository"
)

type UserService interface {
	CreateUser(userRequest *model.UserRequest) (*model.UserResponse, error)
	GetUsers() (*[]model.UserResponse, error)
	GetUser(id uint) (*model.UserResponse, error)
	UpdateUser(id uint, userReq *model.UserRequest) (*model.UserResponse, error)
	DeleteUser(id uint) error
	MigrateUser() error
}

type userService struct {
	userRepository repository.UserRepository
}

func NewUserService(userRepository repository.UserRepository) UserService {
	return userService{userRepository: userRepository}
}

func (s userService) CreateUser(userRequest *model.UserRequest) (*model.UserResponse, error) {
	user := model.ToUsers(userRequest)
	save, err := s.userRepository.Save(user)
	if err != nil {
		return nil, err
	}
	userResponse := model.ToUserResponse(save)
	return userResponse, nil
}

func (s userService) GetUsers() (*[]model.UserResponse, error) {
	users, err := s.userRepository.FindAll()
	if err != nil {
		return nil, err
	}

	userResponses := make([]model.UserResponse, 0)
	for _, user := range *users {
		response := model.ToUserResponse(&user)
		userResponses = append(userResponses, *response)
	}
	return &userResponses, nil
}

func (s userService) GetUser(id uint) (*model.UserResponse, error) {
	user, err := s.findById(id)
	if err != nil {
		return nil, err
	}
	return model.ToUserResponse(user), nil
}

func (s userService) UpdateUser(id uint, userReq *model.UserRequest) (*model.UserResponse, error) {
	foundUser, err := s.findById(id)
	if err != nil {
		return nil, err
	}

	foundUser.Name = userReq.Name
	foundUser.LastName = userReq.LastName
	foundUser.Age = userReq.Age

	savedUser, err := s.userRepository.Save(foundUser)
	if err != nil {
		return nil, err
	}

	return model.ToUserResponse(savedUser), err
}
func (s userService) DeleteUser(id uint) error {
	return s.userRepository.DeleteById(id)
}

func (s userService) findById(id uint) (*model.User, error) {
	return s.userRepository.FindById(id)
}

func (s userService) MigrateUser() error {
	return s.userRepository.Migrate()
}
