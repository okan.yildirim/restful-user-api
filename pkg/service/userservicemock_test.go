package service

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"restful-user-api/pkg/model"
	"testing"
)

func TestUserService_CreateUser_With_Mock(t *testing.T) {
	mockRepository := new(MockRepository)
	userService := NewUserService(mockRepository)

	request := model.UserRequest{
		Name:     "okan",
		LastName: "yildirim",
		Age:      27,
	}

	userEntity := model.User{Name: request.Name, LastName: request.LastName, Age: request.Age}
	mockRepository.On("Save").Return(&userEntity, nil)

	user, err := userService.CreateUser(&request)

	mockRepository.AssertExpectations(t)

	assert.Nil(t, err)
	assert.NotNil(t, user)
	assert.Equal(t, request.Name, user.Name)
	assert.Equal(t, request.LastName, user.LastName)
	assert.Equal(t, request.Age, user.Age)
}

func TestUserService_CreateUser_WhenErrorOccurred_With_Mock(t *testing.T) {
	mockRepository := new(MockRepository)
	userService := NewUserService(mockRepository)

	mockRepository.On("Save").Return(nil, errors.New("error user crud operation"))

	request := model.UserRequest{
		Name:     "error",
		LastName: "yildirim",
		Age:      27,
	}
	user, err := userService.CreateUser(&request)

	mockRepository.AssertExpectations(t)

	assert.NotNil(t, err)
	assert.Nil(t, user)
	assert.Equal(t, "error user crud operation", err.Error())
}

func TestUserService_GetUsers_With_Mock(t *testing.T) {
	mockRepository := new(MockRepository)
	userService := NewUserService(mockRepository)

	users := []model.User{{
		Id:       1,
		Name:     "okan",
		LastName: "yildirim",
		Age:      27,
	}}
	mockRepository.On("FindAll").Return(&users, nil)

	fetchedUsers, err := userService.GetUsers()

	mockRepository.AssertExpectations(t)
	assert.Nil(t, err)
	assert.Equal(t, len(*fetchedUsers), 1)
	assert.Equal(t, (*fetchedUsers)[0].Name, users[0].Name)
	assert.Equal(t, (*fetchedUsers)[0].LastName, users[0].LastName)
	assert.Equal(t, (*fetchedUsers)[0].Age, users[0].Age)

}

func TestUserService_GetUser_With_Mock(t *testing.T) {
	mockRepository := new(MockRepository)
	userService := NewUserService(mockRepository)

	user := model.User{
		Id:       1,
		Name:     "okan",
		LastName: "yildirim",
		Age:      27,
	}
	mockRepository.On("FindById").Return(&user, nil)

	fetchedUser, err := userService.GetUser(user.Id)
	assert.Nil(t, err)
	assert.NotNil(t, fetchedUser)

	assert.Equal(t, user.Name, fetchedUser.Name)
	assert.Equal(t, user.LastName, fetchedUser.LastName)
	assert.Equal(t, user.Age, fetchedUser.Age)
}

func TestUserService_GetUser_WhenErrorOccurred_With_Mock(t *testing.T) {
	mockRepository := new(MockRepository)
	userService := NewUserService(mockRepository)

	mockRepository.On("FindById").Return(nil, errors.New("error user crud operation"))
	fetchedUser, err := userService.GetUser(5)
	assert.NotNil(t, err)
	assert.Nil(t, fetchedUser)

	assert.Equal(t, "error user crud operation", err.Error())
}

func TestUserService_DeleteUser_With_Mock(t *testing.T) {
	mockRepository := new(MockRepository)
	userService := NewUserService(mockRepository)

	mockRepository.On("Delete").Return(nil)

	err := userService.DeleteUser(1)
	assert.Nil(t, err)

}

type MockRepository struct {
	mock.Mock
}

func (r MockRepository) Save(user *model.User) (*model.User, error) {
	args := r.Called()
	result := args.Get(0)

	if result == nil {
		return nil, args.Error(1)
	}
	return result.(*model.User), args.Error(1)
}

func (r MockRepository) FindAll() (*[]model.User, error) {
	args := r.Called()
	result := args.Get(0)
	return result.(*[]model.User), args.Error(1)
}

func (r MockRepository) FindById(id uint) (*model.User, error) {
	args := r.Called()
	result := args.Get(0)
	if result == nil {
		return nil, args.Error(1)
	}
	return result.(*model.User), args.Error(1)
}

func (r MockRepository) DeleteById(id uint) error {
	return nil
}

func (r MockRepository) Migrate() error {
	return nil
}
