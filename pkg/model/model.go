package model

import "time"

type UserRequest struct {
	Name string `json:"name"`
	LastName string `json:"lastName"`
	Age int `json:"age"`
}

type UserResponse struct {
	Id uint `json:"id"`
	Name string `json:"name"`
	LastName string `json:"lastName"`
	Age int `json:"age"`
}

type User struct {
	Id uint `gorm:"primary_key" json:"id"`
	CreatedAt time.Time  `json:"created_at"`
	UpdatedAt time.Time  `json:"updated_at"`
	Name string `json:"name"`
	LastName string `json:"lastName"`
	Age int `json:"age"`
}

func ToUsers(request *UserRequest) *User {
	return &User{
		Name:      request.Name,
		LastName:  request.LastName,
		Age:       request.Age,
	}
}

func ToUserResponse(users *User) *UserResponse {
	return &UserResponse{
		Id:       users.Id,
		Name:     users.Name,
		LastName: users.LastName,
		Age:      users.Age,
	}
}

