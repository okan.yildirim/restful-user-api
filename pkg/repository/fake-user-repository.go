package repository

import (
	"errors"
	"restful-user-api/pkg/model"
)

var errorMessage = "error user crud operation"

type fakeRepository struct {
	userMap map[uint]model.User
}

func NewMockRepository() UserRepository {
	return fakeRepository{userMap: make(map[uint]model.User)}
}

func (r fakeRepository) Save(user *model.User) (*model.User, error) {
	if user.Name == "error" {
		return nil, errors.New(errorMessage)
	}
	r.userMap[user.Id] = *user
	return user, nil
}

func (r fakeRepository) FindAll() (*[]model.User, error) {
	users := []model.User{}
	for _, user := range r.userMap {
		users = append(users, user)
	}
	return &users, nil
}

func (r fakeRepository) FindById(id uint) (*model.User, error) {
	user, ok := r.userMap[id]
	if ok {
		return &user, nil
	}
	return nil, errors.New(errorMessage)
}

func (r fakeRepository) DeleteById(id uint) error {
	delete(r.userMap, id)
	return nil
}

func (r fakeRepository) Migrate() error {
	return nil
}
