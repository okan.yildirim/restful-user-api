package repository

import (
	"github.com/jinzhu/gorm"
	"restful-user-api/pkg/model"
)

type UserRepository interface {
	Save(user *model.User) (*model.User, error)
	FindAll() (*[]model.User, error)
	FindById(id uint) (*model.User, error)
	DeleteById(id uint) error
	Migrate() error
}

type repo struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) UserRepository {
	return repo{db: db}
}

func (r repo) Save(user *model.User) (*model.User, error) {
	err := r.db.Save(user).Error
	return user, err
}

func (r repo) FindAll() (*[]model.User, error) {
	var users []model.User
	err := r.db.Find(&users).Error
	return &users, err
}

func (r repo) FindById(id uint) (*model.User, error) {
	user := new(model.User)
	err := r.db.Where("id = ?", id).First(user).Error
	return user, err
}

func (r repo) DeleteById(id uint) error {
	err := r.db.Delete(&model.User{Id: id}).Error
	return err
}

func (r repo) Migrate() error {
	return r.db.AutoMigrate(&model.User{}).Error
}
